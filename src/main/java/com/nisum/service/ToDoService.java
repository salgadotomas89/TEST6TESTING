package com.nisum.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;

import com.nisum.dao.ToDoDao;
import com.nisum.model.ToDo;

public class ToDoService {
	private ToDoDao hacerDao;
	private ToDo hacer;
	@Before
	public void Setup(){
		hacer = new ToDo();
	}
	
	@Autowired
	public ToDoService(ToDoDao hacerDao){ 
		this.hacerDao = hacerDao; 
	
	}
	public ToDo obtenerTareaPorHacer(long id) {
		hacer = hacerDao.findOne(id);
		return hacer;
	}
	public ToDo obtenerTareaPorHacer(String categoria) {
		hacer = hacerDao.findCategoria(categoria);
		return hacer;
	}
	public ToDo obtenerTareaPorHacer(Boolean estado) {
		hacer = hacerDao.findEstado(estado);
		return hacer;
	}
	public ToDo guardarTarea(ToDo nuevaTarea) {
		return hacerDao.save(nuevaTarea);
	}
	public void eliminarToDo(long id) {
		hacerDao.delete(hacer.getId());
		
	}
	public List<ToDo> obtenerTareas() {
		List<ToDo> listaXhacer = new ArrayList<ToDo>();
		listaXhacer= (List<ToDo>) hacerDao.findAll();
		return listaXhacer;
	}
	
	
	
	

}
