package com.nisum.dao;

import org.springframework.data.repository.CrudRepository;

import com.nisum.model.ToDo;

public interface ToDoDao extends CrudRepository<ToDo, Long> {
	ToDo findCategoria(String string);
	ToDo findEstado(Boolean estado);
	public void delete(long id);
}
