package com.nisum;
import static org.mockito.Matchers.anyLong; 
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.runners.MockitoJUnitRunner;
import com.nisum.dao.ToDoDao;
import com.nisum.model.ToDo;
import com.nisum.service.ToDoService;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

@RunWith(MockitoJUnitRunner.class) 
public class ToDoServiceTest { 	
	@Mock 	
	private ToDoDao toDo; 	
	@InjectMocks 	
	private ToDoService toDoService; 
	private ToDo nuevaTarea,tareaCreada;
	
	@Before
	public void Setup(){
		nuevaTarea = new ToDo();
		tareaCreada = new ToDo();
	}
	
	@Test 	
	public void buscarToDoPorId(){ 	
		//arrange 	
		nuevaTarea.setId(11111); 		
		//Act 		
		when(toDo.findOne(anyLong())).thenReturn(nuevaTarea); 
		 tareaCreada = toDoService.obtenerTareaPorHacer(nuevaTarea.getId()); 	
		//Assert 		
		Assert.assertNotNull(tareaCreada); 	
		Assert.assertEquals(nuevaTarea,tareaCreada); 
	} 
	
	@Test 	
	public void buscarToDoPorCategoria(){ 	
		//arrange 	
		nuevaTarea.setCategoria("importante"); 		
		//Act 		
		when(toDo.findCategoria("importante")).thenReturn(nuevaTarea); 
		 tareaCreada = toDoService.obtenerTareaPorHacer(nuevaTarea.getCategoria()); 	
		//Assert 		
		Assert.assertNotNull(tareaCreada); 	
		Assert.assertEquals(nuevaTarea,tareaCreada); 
	} 
	
	@Test 	
	public void buscarToDoPorEstado(){ 	
		//arrange 	
		nuevaTarea.setEstado(true);; 		
		//Act 		
		when(toDo.findEstado(true)).thenReturn(nuevaTarea); 
		 tareaCreada = toDoService.obtenerTareaPorHacer(nuevaTarea.getEstado()); 	
		//Assert 		
		Assert.assertNotNull(tareaCreada); 	
		Assert.assertEquals(nuevaTarea,tareaCreada); 
	} 
	
	@Test
	public void guardarToDo(){
		//arrange 	
		//act
		when(toDo.save(nuevaTarea)).thenReturn(nuevaTarea);
		 tareaCreada = toDoService.guardarTarea(nuevaTarea); 
		//Assert 		
		Assert.assertNotNull(tareaCreada); 	
		Assert.assertEquals(nuevaTarea,tareaCreada); 
	}
	
	@Test
	public void eliminaToDo(){
		//arrange 	
		nuevaTarea.setId(11111);
		//act
		toDo.delete(nuevaTarea.getId());
		toDoService.eliminarToDo(nuevaTarea.getId());
	
	}
	

	@Test
	public void obtenerTodoLosToDoEnUnaLista(){
		//arrange 	
		List<ToDo> listaXHacer = new ArrayList<ToDo>();;
		//act
		when(toDo.findAll()).thenReturn(listaXHacer);
		List<ToDo> todoPrueba = toDoService.obtenerTareas(); 
		//Assert 		
		Assert.assertNotNull(todoPrueba); 	
		Assert.assertEquals(listaXHacer,todoPrueba); 
	}
	
	
}

